# BSD 3-Clause License
#
# Copyright (c) 2021, Fabian Ellermann
# All rights reserved.

from KillableThreads import Initialization, BaseProcess
import time

print("--------------------------------------------------")
print("Starting very important Init Job")

processing = Initialization()
processing._start()
print(processing.is_alive())

print("Stopping normaly the not anymore important Init Job")

processing.stop()
print(processing.is_alive())
processing.join()

print(processing.is_alive())

print("--------------------------------------------------")
print("Starting very important Init Job")

processing = Initialization()
processing._start()
print(processing.is_alive())

print("Something incredibly bad happend we must kill the important Init Job")

processing.terminate()
print(processing.is_alive())
processing.join()


print("--------------------------------------------------")
print(processing.is_alive())

print(">>> Killed the very bad one. Im still running to do important things")
