# BSD 3-Clause License
#
# Copyright (c) 2021, Fabian Ellermann
# All rights reserved.

import threading
import ctypes
import inspect

import datetime
import time


class BaseProcess(threading.Thread):
    """Base class as interface for jobs with functions that
       must be implemented.
    """

    def _async_raise(self,tid, exctype):
        """raises the exception, performs cleanup if needed"""
        if not inspect.isclass(exctype):
            raise TypeError("Only types can be raised (not instances)")
        res = ctypes.pythonapi.PyThreadState_SetAsyncExc(ctypes.c_long(tid), ctypes.py_object(exctype))
        if res == 0:
            raise ValueError("invalid thread id")
        elif res != 1:
            # """if it returns a number greater than one, you're in trouble,
            # and you should call it again with exc=NULL to revert the effect"""
            ctypes.pythonapi.PyThreadState_SetAsyncExc(tid, 0)
            raise SystemError("PyThreadState_SetAsyncExc failed")

    def _get_my_tid(self):
        """determines this (self's) thread id"""
        if not self.is_alive():
            raise threading.ThreadError("the thread is not active")

        # do we have it cached?
        if hasattr(self, "_thread_id"):
            return self._thread_id

        # no, look for it in the _active dict
        for tid, tobj in threading._active.items():
            if tobj is self:
                self._thread_id = tid
                return tid

        raise AssertionError("could not determine the thread's id")

    def raise_exc(self, exctype):
        """raises the given exception type in the context of this thread"""
        self._async_raise(self._get_my_tid(), exctype)

    def terminate(self):
        """raises SystemExit in the context of the given thread, which should
        cause the thread to exit silently (unless caught)"""
        self.raise_exc(SystemExit)

    def __init__(self):
        """Initialize job in thread and set up events.
        """
        super().__init__()
        #self.thread = threading.Thread(target=self.run)
        self.event = threading.Event()

    def _start(self):
        """Start the thread
        """
        self.start()

    def stop(self):
        """Trigger event and stop the thread
        """
        self.event.set()
        self.abort()

    def abort(self):
        """Abort function, that is declarated but has to be
           implemented in each job individually.

        Raises:
            NotImplementedError: This error is raised if the inheriting class
            does not implement this method.
        """
        raise NotImplementedError

    def run(self):
        """Run function, that is declarated but has to be
           implemented in each job individually.

        Raises:
            NotImplementedError: This error is raised if the inheriting class
            does not implement this method.
        """
        raise NotImplementedError


class Initialization(BaseProcess):
    def __init__(self):
        super().__init__()
        self.i = 1000

    def run(self):
        while not self.event.is_set() and self.i > 0:
            self.i -= 1
            print(str(self.i)+"\n")

        print(datetime.datetime.now())
        time.sleep(5)
        print(datetime.datetime.now())
        print("Terminated gracefully!")

    def abort(self):
        pass

class Error(Exception):
    """Base class for other exceptions
    """
    pass


class ThreadIsRunning(Error):
    """Raised when the thread is still running
    """
    pass

